#!/bin/bash

while [ 1=1 ]
do
  echo "$(tput setaf 2)sleep$(tput sgr 0)"
  sleep 11
  echo "$(tput setaf 1)3$(tput sgr 0)"
  sleep 1
  echo "$(tput setaf 3)2$(tput sgr 0)"
  sleep 1
  echo "$(tput setaf 2)1$(tput sgr 0)"
  sleep 1
  echo "$(tput setaf 2)start$(tput sgr 0)"
  sleep 1
  #start timer
  SECONDS=0
  cd ~/code/darknet

  #copy image from camera
  rsync -avz -e ssh cam@10.131.91.78:photos/*.jpg ~/code/darknet/data/

  #get the exif date from the image
  var=$(exiftool -dateFormat "%Y:%m:%d %H:%M:%S" -fileinodechangedate /home/linux/code/darknet/data/room.jpg)
  #var=$(exiftool -fileinodechangedate /home/linux/code/darknet/data/room.jpg)

  #make a copy of the file
  cp /home/linux/code/darknet/data/room.jpg /home/linux/code/darknet/data/room1.jpg
  #rotate picture
  ffmpeg -y -i /home/linux/code/darknet/data/room1.jpg -vf transpose=1 /home/linux/code/darknet/data/room1.jpg

  #removes (((File Inode Change Date/Time     :))) 2019:04:29 11:03:56
  var=${var#* :}
  #var=${var%+03:00}

  #run yolo ai person detection (35% smallest)
  ./darknet detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights -i 0 -thresh 0.35 -ext_output data/room1.jpg > /home/linux/code/unformated.txt
  #cat /home/linux/code/unformated.txt | sed 's/^.*\(Predicted.*milli\).*$/\1/' > /home/linux/code/debug.txt
  #add to degug the lenght of yolo process
  cat /home/linux/code/unformated.txt | grep -o 'Predicted.*milli-seconds.' > /home/linux/code/debug.txt
  #copy to results.txt and filter to only give persons
  cat /home/linux/code/unformated.txt | grep person | sed 's/\%.*/%/' > /home/linux/code/result.txt
  #./darknet detector test cfg/coco.data cfg/yolov3.cfg yolov3.weights -i 0 -thresh 0.20 -dont_show -ext_output data/room.jpg | sed 's/\%.*/%/' >> /home/linux/code/result.txt




  #get the current date/time and store it in currenttime
  currenttime=$(date +"%D-%H:%M:%S")
  #get the current date/time to compare it with the picture exif date
  now=$(date +%Y:%m:%d:%H:%M:%S)
  updategraph=${var}
  now="$(echo $now | sed 's/://g')"
  updategraph="$(echo $updategraph | sed 's/ //g')"
  updategraph="$(echo $updategraph | sed 's/://g')"
  time="$(($now-$updategraph))"
  #compare the time with the exif time, if its more than 60 seconds reboot camera
  if [ $time -le 60 ]
  then
    #uptopdate
    uptodate=2
  	printf "${currenttime} 2\n" >> /home/linux/code/updategraph
  else
    #not uptopdate
    uptodate=1
  	printf "${currenttime} 1\n" >> /home/linux/code/updategraph
    ssh -t root@10.131.91.78 'reboot'
  fi
  gnuplot /home/linux/code/test2.pg > /home/linux/code/test3.png


  echo -e "<link href="pulsate.css" rel="stylesheet" type="text/css" />\n" > ~/code/updatetime.html
  if [ $uptodate = 2 ]
  then
    echo '<h1 "class=pulsate" style="font-size:100%;text-align:right;font-family:serif;">' >> ~/code/updatetime.html
    echo > /home/linux/code/pulse.css
  else
    echo '<h1 style="font-size:100%;text-align:right;font-family:serif;color:rgb(239, 59, 36);">' >> ~/code/updatetime.html
    cat /home/linux/code/pulsate.css > /home/linux/code/pulse.css
  fi
  echo Updated >> /home/linux/code/updatetime.html
  echo ${var} >> /home/linux/code/updatetime.html
  echo "</h1>" >> /home/linux/code/updatetime.html


  howmanypersons=$(grep -o 'person' ~/code/result.txt | wc -l)
  printf "${currenttime} ${howmanypersons}\n" >> /home/linux/code/graph
  gnuplot /home/linux/code/test.pg > /home/linux/code/test1.png

  processpower=$(cat /home/linux/code/unformated.txt | grep -o -P '(?<=Predicted in ).*(?=milli)')
  printf "${currenttime} ${processpower}\n" >> /home/linux/code/processgraph
  gnuplot /home/linux/code/test1.pg > /home/linux/code/test2.png


  echo -e "<link href="txtstyle.css" rel="stylesheet" type="text/css" />\n" > /home/linux/code/total.html
  echo '<h1 style="font-size:900%;text-align:center;font-family:Arial;">' >> /home/linux/code/total.html
  grep -o 'person' /home/linux/code/result.txt | wc -l >> /home/linux/code/total.html
  echo "</h1>" >> /home/linux/code/total.html

  echo -e "<link href="txtstyle.css" rel="stylesheet" type="text/css" />\n" > /home/linux/code/result.html
  echo '<h1 style="font-size:105%;color:black;font-family:Arial;">' >> /home/linux/code/result.html
  grep person /home/linux/code/result.txt >> /home/linux/code/result.html
  echo "</h1>" >> /home/linux/code/result.html

  echo -e "<link href="txtstyle.css" rel="stylesheet" type="text/css" />\n" > /home/linux/code/debug1.html
  echo '<h1 style="font-size:120%;color:black;">' >> /home/linux/code/debug1.html
  cat /home/linux/code/debug.txt >> /home/linux/code/debug1.html
  echo "</h1>" >> /home/linux/code/debug1.html

  statusone=$(cat /home/linux/code/updategraph | grep -w 1 | wc -l)
  statustwo=$(cat /home/linux/code/updategraph | grep -w 2 | wc -l)
  statustotal=$(echo $statusone + $statustwo | bc)
  echo offline: $((100*$statusone/$statustotal | bc))% > /home/linux/code/prosentti/status1.html
  echo online: $((100*$statustwo/$statustotal | bc))% > /home/linux/code/prosentti/status2.html



  one=$(cat /home/linux/code/graph | grep -w 1 | wc -l)
  two=$(cat /home/linux/code/graph | grep -w 2 | wc -l)
  three=$(cat /home/linux/code/graph | grep -w 3 | wc -l)
  four=$(cat /home/linux/code/graph | grep -w 4 | wc -l)
  five=$(cat /home/linux/code/graph | grep -w 5 | wc -l)
  six=$(cat /home/linux/code/graph | grep -w 6 | wc -l)
  seven=$(cat /home/linux/code/graph | grep -w 7 | wc -l)
  eight=$(cat /home/linux/code/graph | grep -w 8 | wc -l)
  nine=$(cat /home/linux/code/graph | grep -w 9 | wc -l)
  total=$(echo $one + $two + $three + $four + $five + $six + $seven + $eight + $nine | bc)
  echo 1: $one times $((100*$one/$total | bc))% > /home/linux/code/prosentti/1.html
  echo 2: $two times $((100*$two/$total | bc))% > /home/linux/code/prosentti/2.html
  echo 3: $three times $((100*$three/$total | bc))% > /home/linux/code/prosentti/3.html
  echo 4: $four times $((100*$four/$total | bc))% > /home/linux/code/prosentti/4.html
  echo 5: $five times $((100*$five/$total | bc))% > /home/linux/code/prosentti/5.html
  echo 6: $six times $((100*$six/$total | bc))% > /home/linux/code/prosentti/6.html
  echo 7: $seven times $((100*$seven/$total | bc))% > /home/linux/code/prosentti/7.html
  echo 8: $eight times $((100*$eight/$total | bc))% > /home/linux/code/prosentti/8.html
  echo 9: $nine times $((100*$nine/$total | bc))% > /home/linux/code/prosentti/9.html


  printf "${currenttime} ${timetook}\n" >> /home/linux/code/timetookgraph
  gnuplot /home/linux/code/test3.pg > /home/linux/code/test4.png
  timetook=$(echo $SECONDS)
  echo "$(tput setaf 2)Finished in: $timetook + 15 seconds$(tput sgr 0)"

done
